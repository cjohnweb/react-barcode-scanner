var express = require('express');
var router = express.Router();
var path = require('path');

// files
router.use('/assets', express.static(path.join(__dirname, 'assets')));

// jade/pug template
router.get('/', (req, res) => {
    res.render(path.join(__dirname, 'view.jade'));
});

module.exports = router;