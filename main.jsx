import React from 'react';

// Import the scanner
import Scanner from './scanner';

/**
 * scannerExample is a react component designed to show you how to use the scanner.jsx component.
 * Simply follow the directions in the comments and copy out the necessary parts into your project
 * in the appropriate places.
 */

class scannerExample extends React.Component {
  constructor(props) {
    super(props);

  }

  render() {

    return (<div>
      <div>
        <h1>Scanner Example</h1><br />
      </div>
      <div style={{ width: "500px", margin: "0px auto" }}>
        <h3>React Barcode Scanner</h3><br />
        <p>
          Hello! Thanks for checking out the React Barcode Scanner. I would love to hear how
          this scanner has helped you in your own project(s). Please send me your feedback!
          You can reach me through gitlab at <a href='https://gitlab.com/cjohnweb/react-barcode-scanner'>
            https://gitlab.com/cjohnweb/react-barcode-scanner</a> or email
          me <a href='mailto:cjohnweb@gmail.com'>mailto:cjohnweb@gmail.com</a>.
        </p>
        <p>
          There are 2 ways you might want to impliment this scanner into your project:
        </p>
        <ul style={{ margin: "10px" }}>
          <li style={{ margin: "10px" }}>
            Pass data from scanner.jsx back to main.jsx this.state object.
            Create a method in this file called passScannerData(scanner) and pass it
            down to the Scanner component as a prop. In scanner.jsx pass the scanner
            object to this.props.passScannerData(scanner) and set the scanner object
            is state or what have you. Then use this new scanner object to access the scanner data.
          </li>
          <li style={{ margin: "10px" }}>
            Include the scanner.jsx file at the root of where you need the scanner,
            and include your child component in the scanner.jsx file's render. Be
            sure to pass the scanner object down as a prop.
          </li>
        </ul>
        <p>
          Which ever way you decide to use the scanner, you ultimately end up with the
          scanner object in your component. It looks something like this.
          </p>

        <div style={{ backgroundColor: "#efefef" }}>
          <pre>{`
            let scanner = {
              data: {
                currentScan: {
                  nameYourBarcode: "xxxxxxxxx",
                  deviceSerial: "xxxxxxxxx",
                  catchAllScans: "xxxxxxxxx"
                },
                lastScan: {
                  nameYourBarcode: "xxxxxxxxx",
                  deviceSerial: "xxxxxxxxx",
                  catchAllScans: "xxxxxxxxx"
                }
              },
              functions: {
                moveCurrentScanToLastScan: function()...,
                clearAllState:  function()...,
                setScannerActive:  function()...
              }
            };
          `}</pre>
        </div>
        <p>
          In your component where you have access to the scanner object, you'll most likely want
          to check for changes to the scanner.data.currentScan object inside
          ComponentsWillReceiveProps(nextProps). From there, it's up to you to decide how you'll
          want to use the scanner. After scanning 1 or more barcodes, do what you need to do
          with the data (save it, load an items name and price in a Point of Sale systm, show
          product data to a customer, etc). Then call scanner.functions.moveCurrentScanToLastScan()
          to move the current scan to the lastScan. You can choose what to do with the lastScan
          object - you can display it or simply ignore it.
          </p>

        <ul style={{ margin: "10px" }}>
          <li style={{ margin: "10px" }}>moveCurrentScanToLastScan() - If you ever want to scan multiple barcodes, they are
              all stored in the scanner.data.currentScan object. When you are done with those codes,
              you can save them or whatever, and then use this method to move currentScan to lastScan.
              This allows you to keep track of the last scanned section.</li>
          <li style={{ margin: "10px" }}>clearAllState() - Wipe out the currentScan and the lastScan</li>
          <li style={{ margin: "10px" }}>setScannerActive() - Pass this a false to disable the scanner, and a true to re-enable the scanner.</li>
        </ul>
        <div>
          <Scanner />
        </div>
      </div>
    </div>
    );

  }
}

export default scannerExample;