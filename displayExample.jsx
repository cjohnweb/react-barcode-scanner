import React from 'react';

class DisplayExample extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      inputBuffer: "",
      currentScan: {},
      lastScan: {}
    };

  }


  componentWillReceiveProps(nextProps) {
    let nextScan = nextProps.scanner.data.currentScan;
    if (nextScan.catchAllScans && deviceSerial) {
      // Load barcode into state and clear the scanner
      // or dispatch the scan and load the store up with info
      // or whatever you want. Scans can also be used for
      // navigation, setting administrative access, etc.
      // When you're done, you might simple copy the currentScan
      // into the lastScan (which also clears the currentScan out):
      this.props.scanner.functions.moveCurrentScanToLastScan();
    }
  }

  render() {

    let scanner = this.props.scanner.data;

    scanner.currentScan = Object.keys(scanner.currentScan)
      .map((name) => {
        let scan = scanner.currentScan[name];
        return (<span style={{ margin: "10px" }}>{name}: {scan}</span>);
      })

    scanner.lastScan = Object.keys(scanner.lastScan)
      .map((name) => {
        let scan = scanner.currentScan[name];
        return (<span style={{ margin: "10px" }}>{name}: {scan}</span>);
      })

    return (<div style={{ margin: "10px" }}>
      <h3>Scan a barcode and observe the results below:</h3><br />
      <div style={{ margin: "10px" }}>Current Scan: {scanner.currentScan}</div>
      <div style={{ margin: "10px" }}>Last Scan: {scanner.lastScan}</div>
    </div>
    );
  }
}

export default DisplayExample;

