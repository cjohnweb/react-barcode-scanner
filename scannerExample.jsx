import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from './store';
import Main from './main';

window.addEventListener("DOMContentLoaded", () => {
    let root = document.getElementById("scanner");
    ReactDOM.render(<Provider store={store}><Main /></Provider>, root);
});

