import React from 'react';
import { connect } from 'react-redux';

// Pages

import DisplayExample from './displayExample';
// import YourProject from './main.jsx';


/*********************************************************
 * React (Conventional) Barcode Scanner
 * By John Minton <cjohnweb@gmail.com>, January 5th, 2018
 * https://gitlab.com/cjohnweb/react-barcode-scanner.git
 * 
 * This component is made to be a drop in barcode scanner object.
 *
 * Your main react file should point to the scanner, and the scanner 
 * point to your main project file where you want the scanner to be 
 * used. 
 * 
 * Then follow these 3 steps to configure this scanner file:
 * 
 * -> Import your project file above
 *
 * -> Setup your barcodes pattern detection here for simplicity or use 
 *    the 'detectAllOtherBarcodes' to catch every code scanned.
 *
 * -> Make sure you pass the scanner object down as props to your jsx file.
 * 
 *    Please feel free to contribute or give me feedback on the project via gitlab
 * 
 */

class Scanner extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      inputBuffer: "",
      currentScan: {},
      lastScan: {}
    };

    // Toggle the scanners ability to detect barcode patterns.
    this.scanneractive = false; // true == Scanner Enabled

    // Capture Scanner Input
    this.processInput = this.processInput.bind(this);

    // Barcode Detection Patterns
    this.detectBarcode1 = this.detectBarcode1.bind(this);
    this.detectBarcode2 = this.detectBarcode2.bind(this);
    this.detectAllOtherBarcodes = this.detectAllOtherBarcodes.bind(this);

    // For child component(s) to control the scanner
    this.clearAllState = this.clearAllState.bind(this); // Reset the scanner
    this.moveCurrentScanToLastScan = this.moveCurrentScanToLastScan.bind(this); // Transfer the currentScan object to the lastScan Object
    this.setScannerActive = this.setScannerActive.bind(this); // Activate or Deactivate the scanner
  }

  componentDidMount() {
    // Passing every keypress to the this.processInput. After all, barcode scanners are basically fancy keyboards.
    document.addEventListener("keydown", this.processInput, false);
    this.scanneractive = true; // Turn the scanner on now
  }

  componentWillUnmount() {
    // Stop listening to keypresses
    this.scanneractive = false; // Turn the scanner off now
    document.removeEventListener("keydown", this.processInput, false);
  }

  clearAllState() {
    let inputBuffer = ""; // clear the inputBuffer
    let currentScan = {}; // clear the currentScan
    let lastScan = {}; // clear the last scan
    this.setState({ inputBuffer, currentScan, lastScan }); // Boom baby
  }

  moveCurrentScanToLastScan() {
    let currentScan = Object.assign({}, this.state.currentScan);
    let lastScan = currentScan; // clear the last scan
    lastScan.date = new Date().toISOString();
    currentScan = {};
    this.setState({ currentScan, lastScan });
  }

  setScannerActive(boolean) {
    if (boolean) {
      this.scanneractive = true;
    } else {
      this.scanneractive = false;
    }
  }

  processInput(event) {

    // Set a timeout on character input.
    // Why? To protect the "scanner" capabilities.
    // Scanners are basicaly keyboards. They output text to a computer 
    // as if someone was typing it out on a keyboard really fasts.
    // When you scan a code they give you the text from the code usually 
    // followed by a tab or enter keypress (depending on your particular 
    // scanner). Scanners also input characters really fast - faster than 
    // you can mash on the keyboard is most cases.
    // So we do some funny business with the timeouts here so that only 
    // a scanner can type fast enough to load the inputBuffer of the scan.
    // This way a user would not be able to fake a scan using the keyboard.
    // You shouldn't need to touch any of this except at the very bottom of
    // this function where you declaire the kinds of barcode patterns you 
    // are looking to match.

    if (this.state.entryTimeout != null) {
      clearTimeout(this.state.entryTimeout);
      // Some scanners put a tab at the end of scanning. This will cause your browsers cursor to jump with every 
      // scan, eventually moving the cursor into the browsers address bar, where your scans will be captured by 
      // the address bar not be captured by the page.
      if (event.key == 'Tab' || event.key == 'Enter') {
        event.preventDefault();
      }
    } // Clear the timeout everytime a key is pressed
    this.setState({
      entryTimeout: setTimeout(() => {
        let inputBuffer = this.state.inputBuffer;
        inputBuffer = "";
        this.setState({ entryTimeout: null, inputBuffer });
      }, 40)
    });

    // Only update the inputBuffer if the 'keypress' is a letter or number, not tab, space or enter.
    // We are not destroying the keypress event, so you can still reference it elsewhere.
    let newBuffer = this.state.inputBuffer;
    if (event.key != 'Tab' && event.key != 'Enter' && event.code != 'Space' && event.key != 'Shift') {
      newBuffer = newBuffer + event.key;
    }

    // Apply the modifications from newBuffer to inputBuffer
    let inputBuffer = newBuffer;

    // Update the inputBuffer state, then see callback
    this.setState({ inputBuffer }, () => {
      // If the last key was a tab, enter or space, we don't load that key stroke into the inputBuffer
      // Instead it will trigger us to check the inputBuffer to see if it matches any Barcode / Serial number patterns

      if (event.key == 'Tab' || event.key == 'Enter' || event.code == 'Space') {
        // Determine which code was scanned
        // The longest serials should be checked first
        // If one of these functions finds a match, it will set the serial in the appropriate state and clear the inputBuffer
        if (inputBuffer.length > 0) {
          console.log("Scanner: ", inputBuffer);
        }

        // Any barcodes that you want detected regardless of the this.scanneractive 'scanner lock' go here
        // this.detectBarcode1(); // ie a navigation barcode might go here

        // The scanner lock will short circuit this function here
        if (!this.scanneractive) { return; }

        // These barcodes will not be picked up by the scanner if this.scanneractive == false
        this.detectBarcode1();
        this.detectBarcode2();
        this.detectAllOtherBarcodes();
        // Add your specific barcode functions here
        // Use the this.detectBarcode1 method below 
        // as a template.
      }
    });
  }

  // Primary Scanner-based Navigation Detection
  detectBarcode1() {
    // currentScan state object holds all recognized barcodes
    let currentScan = Object.assign({}, this.state.currentScan);
    // This is the state of the current scan
    let inputBuffer = this.state.inputBuffer;
    // The pattern of the barcode we want to revognize
    let pattern = /key:\d{5,10}$/; // looks for scanned codes that are 'key:' followed by 5 or 6 or 7 or 8 or 9 or 10 digits
    var result = pattern.test(inputBuffer); // Test the inputBuffer against the pattern and see if there is a match
    if (result) { // If the match was successfull
      currentScan.nameYourBarcode = inputBuffer; // Give your barcode a unique name: currentScan.something
      // The pattern was a match. Copy the input Buffer to the state.currentScan and clear out the inputBuffer
      this.setState({ inputBuffer: "", currentScan });
    }
  }

  detectBarcode2() {
    let currentScan = Object.assign({}, this.state.currentScan);
    let inputBuffer = this.state.inputBuffer;
    let pattern = /device\d{8}$/; // looks for scanned codes that are 'device' followed by 8 digits ie 'device00000000'
    var result = pattern.test(inputBuffer);
    if (result) {
      currentScan.deviceSerial = inputBuffer;
      this.setState({ inputBuffer: "", currentScan });
    }
  }

  detectAllOtherBarcodes() {
    let currentScan = Object.assign({}, this.state.currentScan);
    let inputBuffer = this.state.inputBuffer;
    currentScan.catchAllScans = inputBuffer;
    this.setState({ inputBuffer: "", currentScan });
  }

  render() {

    // Here we create a scanner object that we will pass down as a prop to your project
    // We have define the functions needed to reset scanner and to transition the currentScan to lastScan once you have processed the scan(s).
    // We also pass down the scans themselves, the current scan and the last scan objects from state
    // Pass this object to your project.

    let scanner = {
      data: {
        currentScan: this.state.currentScan,
        lastScan: this.state.lastScan
      },
      functions: {
        moveCurrentScanToLastScan: this.moveCurrentScanToLastScan,
        clearAllState: this.clearAllState,
        setScannerActive: this.setScannerActive,
      }
    };

    // The page you want to pass the scanner object to
    let content = (<div>
      <DisplayExample scanner={scanner} />
    </div>);

    return (<div>
      {content}
    </div>
    );
  }
}

export default Scanner;

