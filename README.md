# React (Conventional) Barcode Scanner
John Minton <cjohnweb@gmail.com> MintonTechnologies.com

This is a fontend React component desiged to handle the input of a barcode 
scanner.

See the files for example usage (just point a route to the routes file in 
your node server) but you only need the scanner.jsx file to bring the 
scanner into your project. Instructions are commented throughout the jsx 
files.

Scanners are keyboards. They essentially input data exactly the same as if 
you were typing with a keyboard. The only difference is that a barcode scanner 
can type faster than a human can and always puts a tab or enter as the last 
character. So we use this time-factor to filter out scanner vs keyboards 
presses. Then, once we detect a tab or enter after a string of text comes 
through, we do some pattern matching to see if the barcode scanned is one 
we want. If the barcode is valid, we store it under a unique variable name 
in the currentScan object. Then you are free to customize desired.

Please leave feedback here https://gitlab.com/cjohnweb/react-barcode-scanner
